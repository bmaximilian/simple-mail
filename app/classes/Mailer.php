<?php

/**
 * Mailer class for sending mails with uploaded attachments
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Mailer
{

    /**
     * Instance of PHPMailer
     * @var PHPMailer
     */
    private $mail;


    /**
     * Contains recipient data
     * @var
     */
    private $recipient;


    /**
     * Mailer constructor.
     *
     * @param $con : Connection configuration
     * @param $recipient : Recipient data
     */
    function __construct($con, $recipient)
    {
        $this->mail = $this->createMailer($con);
        $this->recipient = $recipient;
    }


    /**
     * Creates instance of PHPMailer with
     * configuration from $con
     *
     * @param $con : Connection configuration
     *
     * @return PHPMailer
     */
    private function createMailer($con)
    {
        $mail = new \PHPMailer();

        $mail->isSMTP();
        $mail->Host = $con['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $con['user'];
        $mail->Password = $con['pass'];
        $mail->SMTPSecure = 'tls';
        $mail->Port = $con['port'];
        $mail->SMTPDebug = 0;
        $mail->setLanguage('de');
        $mail->CharSet = 'UTF-8';

        return $mail;
    }


    /**
     * Sends mail with passed data
     * from POST (sender) and FILES (attachments)
     *
     * @param $data
     *
     * @return bool
     * @throws phpmailerException
     */
    public function sendMail($data)
    {
        if ($this->mail instanceof \PHPMailer) {

            $this->mail->setFrom($data['mail'], $data['name']);
            $this->mail->addAddress($this->recipient['mail'], $this->recipient['name']);

            $this->mail->isHTML(false);

            $this->mail->Subject = $data['subject'];

            $this->mail->Body = $data['message'];
            $this->mail->AltBody = $data['message'];


            foreach ($data['files'] as $file)
                if ($file && $file['name'] && $file['tmp_name'])
                    for ($i = 0; $i < sizeof($file['name']); $i++)
                        if ($file['error'][$i] === UPLOAD_ERR_OK)
                            $this->mail->addAttachment($file['tmp_name'][$i], $file['name'][$i]);

        }

        return $this->mail->send();
    }
}