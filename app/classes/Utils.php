<?php

/**
 * Helper class
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
abstract class Utils
{
    /**
     * Function for parsing an ini file
     *
     * @param $ini_file - name of INI File
     * @param $ini_path - path to INI file (default is bin/)
     * @return array        - returns parsed ini
     */
    public static function getIni($ini_file, $ini_path = "bin/")
    {
        try
        {

            $ini_path = realpath("./" . $ini_path) . "/";

            return strpos($ini_file, '/') ? parse_ini_file($ini_file) : parse_ini_file($ini_path . $ini_file, TRUE);

        } catch (Exception $e) {
            return null;
        }
    }

}