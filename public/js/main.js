/**
 * Script to display the selected files
 * of .mdl-fileinput in an input element
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var fileinputs = document.getElementsByClassName('mdl-fileinput');
for (var i = 0; i < fileinputs.length; i++) {
    fileinputs[i].addEventListener("change", function () {
        var filenames = "";
        for (var k = 0; k < this.files.length; k++) {
            if (k > 0)
                filenames += ", ";
            filenames += this.files[k].name;
        }

        var container = this.parentElement.parentElement;
        for (k = 0; k < container.childNodes.length; k++) {
            if (container.childNodes[k].classList && container.childNodes[k].classList.contains('mdl-fileinput__input'))
                container.childNodes[k].value = filenames;
        }
    })
}


/**
 * Returns data of the mailing form
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
function _getData() {
    return {
        name: document.getElementById('name').value,
        mail: document.getElementById('email').value,
        subject: document.getElementById('subject').value,
        message: document.getElementById('message').value,
        files: document.getElementById('attachments').files
    }
}


/**
 * Appends data to the FormData
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 *
 * @param formData
 * @param data
 * @returns {*}
 */
function _appendData(formData, data) {
    for (var i = 0; i < data.files.length; i++)
        formData.append('files[]', data.files[i]);

    if (data.name)
        formData.append("name", data.name);
    if (data.mail)
        formData.append("mail", data.mail);
    if (data.subject)
        formData.append("subject", data.subject);
    if (data.message)
        formData.append("message", data.message);

    return formData;
}


/**
 * Function to send a mail request
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
function sendMailRequest() {
    document.getElementById('alert').innerHTML = "Lade Dateien hoch...";

    var formData = new FormData();
    var data = _getData();
    
    formData = _appendData(formData, data);
    
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "sendmail.php", true);

    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200)
            document.getElementById('alert').innerHTML = xhr.responseText == "1" ? "Mail wurde verschickt." : "Serverseitiger fehler beim Verschicken der Mail.";
    };

    xhr.send(formData);
}


var btn = document.getElementById("sendMail");
btn.addEventListener("click", sendMailRequest);
btn.addEventListener("touchend", sendMailRequest);