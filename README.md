# Installationsanleitung
Die App benötigt **Composer** und einen Webserver mit mindestens **PHP 5.6**.

Folgender Befehl installiert die Abhängigkeiten:
```shell
composer install
```

## Daten senden
Die Formulardaten sollten in als **POST** Anfrage geschickt werden.
Es ist allerdings auch möglich die Daten über **GET** oder als **Cookie** zu senden.

Beispiel um Daten zu schicken:
```javascript
var formData = new FormData();

formData.append("name", document.getElementById('name').value);
formData.append("mail", document.getElementById('email').value);
formData.append("subject", document.getElementById('subject').value);
formData.append("message", document.getElementById('message').value);
formData.append('files[]', document.getElementById('attachments').files[0]);

var xhr = new XMLHttpRequest();
xhr.open("POST", "sendmail.php", true);
xhr.send(formData);
```

Die dem Anhang hinzuzufügenden Dateien sind als *multipart* Anfrage zu schicken

## Konfiguration
Die Konfigurationsdatei befindet sich unter *bin/config.ini*.
In dieser Datei ist der Zugang zum Mailserver, sowie Daten des Empfängers der Mail einzutragen.

Außerdem sind in der Konfigurationsdatei Platzhalter für eventuell nicht korrekt gesendetet Daten eingetragen.