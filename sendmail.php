<?php
/**
 * Script for sending a mail with attachment to the website owner
 *
 * Configuration in ./bin/config.ini
 * Post data should arrive in array data with name, mail, subject and message
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */

require 'vendor/autoload.php';
require 'app/autoload.php';

$config = Utils::getIni("config.ini");

$mail_data = [
    "name" => (@$_REQUEST['name']) ? @$_REQUEST['name'] : "",
    "mail" => (@$_REQUEST['mail']) ? @$_REQUEST['mail'] : $config['standards']['mail'],
    "subject" => (@$_REQUEST['subject']) ? @$_REQUEST['subject'] : $config['standards']['subject'],
    "message" => (@$_REQUEST['message']) ? @$_REQUEST['message'] : $config['standards']['message'],
    "files" => (@$_FILES ? @$_FILES : [])
];

$mailer = new Mailer($config['mailing'], $config['recipient']);

print $mailer->sendMail($mail_data);